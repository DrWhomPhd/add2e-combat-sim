use add2e_combatsim::{Die, Entity, Simulation};
use clap::{App, Arg};
use failure::{ensure, Error};
use std::env;

fn main() {
    let matches = App::new("AD&D 2e Battle Sim")
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about("Performs combat simulations and provides a success chance based on montecarlo sims")
        .arg(
            Arg::with_name("npc_hp")
                .short("n")
                .long("npc_hd")
                .help("Number of HD NPC Mob has")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("npc_thaco")
                .short("t")
                .long("npc_thaco")
                .help("NPC's THAC0")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("npc_dmg")
                .short("m")
                .long("npc_dmg")
                .help("NPC's Damage in #d#+X")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("npc_ac")
                .long("npc_ac")
                .help("NPC's AC level")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("pc_hp")
                .short("p")
                .long("pc_hp")
                .help("PC's HP")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("pc_thaco")
                .short("c")
                .long("pc_thaco")
                .help("PC's THAC0")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("pc_ac")
                .long("pc_ac")
                .help("PC's AC #")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("pc_dmg")
                .short("g")
                .long("pc_dmg")
                .help("PC's DMG in #d#+X")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("sim_runs")
                .short("s")
                .long("sim_runs")
                .help("Number of Sim Runs.")
                .default_value("20000")
                .takes_value(true),
        )
        .get_matches();

    let (pc_dcnt, pc_die, pc_bonus) =
        match parse_dmg_str(&String::from(matches.value_of("pc_dmg").unwrap())) {
            Ok((x, y, z)) => (x, y, z),
            Err(e) => panic!("Failed to parse PC Dmg string!"),
        };
    let (npc_dcnt, npc_die, npc_bonus) =
        match parse_dmg_str(&String::from(matches.value_of("npc_dmg").unwrap())) {
            Ok((x, y, z)) => (x, y, z),
            Err(e) => panic!("Failed to parse NPC DMG string!"),
        };

    let pc_entity = Entity::new(
        matches.value_of("pc_hp").unwrap().parse::<i32>().unwrap(),
        matches
            .value_of("pc_thaco")
            .unwrap()
            .parse::<i32>()
            .unwrap(),
        matches.value_of("pc_ac").unwrap().parse::<i32>().unwrap(),
        pc_dcnt,
        pc_die,
        pc_bonus,
    );
    let npc_entity = Entity::new(
        matches.value_of("npc_hp").unwrap().parse::<i32>().unwrap(),
        matches
            .value_of("npc_thaco")
            .unwrap()
            .parse::<i32>()
            .unwrap(),
        matches.value_of("npc_ac").unwrap().parse::<i32>().unwrap(),
        npc_dcnt,
        npc_die,
        npc_bonus,
    );

    let mut sim = Simulation::new(
        pc_entity,
        npc_entity,
        matches
            .value_of("sim_runs")
            .unwrap()
            .parse::<i32>()
            .unwrap(),
    );

    let result = sim.full_run();

    println!("PC Success: {:.2}%", result);
}

fn parse_dmg_str(dmg: &String) -> Result<(i32, i32, i32), Error> {
    ensure!(dmg.contains('d'));

    // Split out #d#
    let dmg_split: Vec<&str> = dmg.split('d').collect();

    // Get our first #
    let die_cnt = dmg_split.get(0).unwrap().parse::<i32>()?;

    // Determine if it's d#+X or just d#
    let (die_size, bonus) = if dmg_split.get(1).unwrap().contains('+') {
        let split: Vec<&str> = dmg_split.get(1).unwrap().split('+').collect();
        (
            // d#+X
            split.get(0).unwrap().parse::<i32>()?,
            split.get(1).unwrap().parse::<i32>()?,
        )
    } else {
        // d#+0
        (dmg_split.get(1).unwrap().parse::<i32>()?, 0)
    };

    Ok((die_cnt, die_size, bonus))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_dmg_str() {
        let (cnt, size, bonus) = parse_dmg_str(&String::from("2d4+5")).unwrap();
        assert_eq!(2, cnt);
        assert_eq!(4, size);
        assert_eq!(5, bonus);
        let (cnt, size, bonus) = parse_dmg_str(&String::from("1d6")).unwrap();
        assert_eq!(1, cnt);
        assert_eq!(6, size);
        assert_eq!(0, bonus);
    }

    #[test]
    fn test_single_run() {
        let pc = Entity::new(10, 20, 4, 1, 6, 2);
        let npc = Entity::new(8, 19, 6, 1, 5, 0);
        let mut sim = Simulation::new(pc, npc, 1);
        println!("Single Run Result: {}", sim.single_run());
    }

    #[test]
    fn test_full_run() {
        let pc = Entity::new(10, 20, 4, 1, 6, 1);
        let npc = Entity::new(25, 17, 7, 1, 2, 0);
        let mut sim = Simulation::new(pc, npc, 20000);
        println!("Full Run Result: {:.2}", sim.full_run());
    }
}
