use rand::prelude::*;
use std::error::Error;

pub struct Simulation {
    pc_entity: Entity,
    npc_entity: Entity,
    rounds: i32,
}
impl Simulation {
    pub fn new(pc_entity: Entity, npc_entity: Entity, rounds: i32) -> Simulation {
        Simulation {
            pc_entity,
            npc_entity,
            rounds,
        }
    }
    pub fn single_run(&self) -> bool {
        let mut pc = self.pc_entity.clone();
        let mut npc = self.npc_entity.clone();
        while pc.is_alive() && npc.is_alive() {
            let pc_init = Die(10).roll();
            let npc_init = Die(10).roll();

            if pc_init >= npc_init {
                // PC goes first
                Simulation::sim_step(&mut pc, &mut npc);
            } else {
                // NPC goes first
                Simulation::sim_step(&mut pc, &mut npc);
            }
        }
        pc.is_alive()
    }

    pub fn full_run(&mut self) -> f32 {
        let mut accum = 0.0;
        for _x in 0..self.rounds {
            if self.single_run() {
                accum += 1.;
            }
        }
        accum / (self.rounds as f32)
    }

    fn sim_step(first: &mut Entity, second: &mut Entity) {
        // First half of combat
        let combat_roll = Die(20).roll();
        if second.check_hit(combat_roll, first.get_thaco()) {
            // Hit on Second Entity
            second.take_damage(first.roll_damage());
        }

        // Second half of combat
        let combat_roll = Die(20).roll();
        if first.check_hit(combat_roll, second.get_thaco()) {
            //Hit on First Entity
            first.take_damage(second.roll_damage());
        }
    }
}

#[derive(Clone, Debug)]
pub struct Entity {
    cur_hp: i32,
    max_hp: i32,
    thaco: i32,
    ac: i32,
    dmg_diecnt: i32,
    dmg_die: i32,
    dmg_bonus: i32,
}

impl Entity {
    pub fn new(
        hp: i32,
        thaco: i32,
        ac: i32,
        dmg_diecnt: i32,
        dmg_die: i32,
        dmg_bonus: i32,
    ) -> Entity {
        Entity {
            cur_hp: hp,
            max_hp: hp,
            thaco,
            ac,
            dmg_diecnt,
            dmg_die,
            dmg_bonus,
        }
    }

    pub fn get_thaco(&self) -> i32 {
        self.thaco
    }

    pub fn get_hp(&self) -> i32 {
        self.cur_hp
    }

    pub fn roll_damage(&self) -> i32 {
        Die(self.dmg_die).multi_roll(self.dmg_diecnt) + self.dmg_bonus
    }

    pub fn take_damage(&mut self, damage: i32) {
        self.cur_hp -= damage;
    }

    pub fn check_hit(&self, combat_roll: i32, enemy_thaco: i32) -> bool {
        let hit_num = enemy_thaco - self.ac;
        if combat_roll >= hit_num {
            true
        } else {
            false
        }
    }

    pub fn is_alive(&self) -> bool {
        if self.cur_hp <= 0 {
            false
        } else {
            true
        }
    }
}

pub struct Die(pub i32);
impl Die {
    pub fn roll(&self) -> i32 {
        let mut rng = rand::thread_rng();
        rng.gen_range(1, self.0 + 1)
    }

    pub fn multi_roll(&self, number: i32) -> i32 {
        let mut total = 0;
        for i in 0..number {
            total += self.roll();
        }
        total
    }
}
